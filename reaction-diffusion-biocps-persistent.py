# -*- coding: utf-8 -*-
"""
Created on Fri Jun 15 02:01:37 2018

@author: Demarcus Briers
REF:https://scipython.com/book/chapter-7-matplotlib/examples/the-two-dimensional-diffusion-equation/
"""
import os
import numpy as np
import matplotlib.pyplot as plt
from timeit import default_timer as timer
from scipy.integrate import odeint

def main(dt_user,savepath):
    """
    Main function to run reaction diffusion simulation.
    
    Inputs:
    ------------
    dt_user - user defined time step(hours).
    """
    global nx,ny,dx,dy,D,dt,dx2,dy2,Tcool,Thot,save_freq
    save_state_time = 0.0
    diffusion_time = 0.0
    ode_time = 0.0
   
    ################
    #SPACE
    ###############
    # plate size, 50mmx50mm -> 2inx2in
    w = h = 50.0
    # intervals in x-, y- directions, um
    dx = dy = 1.0
    # Diffusion rate of AHL in 2D agar, cm^2/hr 
    D = 0.15
    Tcool, Thot = 0, 20
    
    
    nx, ny = int(w/dx), int(h/dy)
    dx2, dy2 = dx*dx, dy*dy
    #maximal time step for stable integration of laplace equation.
    dt_max = dx2 * dy2 / (2 * D * (dx2 + dy2))
    if dt_user > dt_max:
        dt = dt_max
    else:
        dt = dt_user
        
    ###############
    # TIME    
    ###############
    
    time_end = 30 #hours
    time_steps = int(time_end/dt)
    save_freq = 2 #hours
    save_times = list(range(0,int(time_end)+1,save_freq)) # Output 4 figures at these timesteps
    assert save_freq >=1, "Save frequency must be greater than 1 for now"
    
    ########################
    # Visualization and Outputs
    #######################
    output_dir = savepath  
    
    
    ########################
    #initil Conditions
    ########################
    #AHL metabolite
    A = np.zeros((nx, ny))
    A = init_spot(A,r=10,cx=15,cy=15,value=20)
    A = init_spot(A,r=10,cx=25,cy=35,value=20)
    A = init_spot(A,r=5,cx=40,cy=30,value=20)
    
    G = 1.0*np.ones((nx, ny))  #GFP protein
    R = 1.0*np.ones((nx, ny))  #lasR protein
    C = 1.0*np.ones((nx, ny)) #lasR-AHL complex

    ################################
    # Run Simulation time steps
    ################################
    #fignum = 0
    #fig = plt.figure()
    #diffusion in hours, reaction in minutes.
    for step_i in list(range(time_steps+1)):
        if step_i*dt>0:
            #########################
            #Reaction Step
            ########################
            t_ode=timer()
            #Y = np.stack((G,A,R,C),axis=0).T.flatten() #must v=be 1D array
            G,A,R,C = integrate([G,A,R,C],dx)
            t_ode_end = timer()-t_ode
            ode_time +=t_ode_end

            ##########################
            #Diffusion Step
            #########################
            t_diffusion=timer()
            A = Diffusion(A)
            t_diffusion_end = timer()-t_diffusion
            diffusion_time+=t_diffusion_end
        
        ############################
        #Save at chosen intervals
        ############################
        if step_i*dt in save_times:
            #fignum += 1
            #ax = fig.add_subplot(240 + fignum)
            t_save = timer()
            print("Saving image for time=%.02f" % float(step_i*dt))
            
            #Save images. Add saves to CSV
            save_state(A,step_i,output_dir,prefix="AHL",minval=0,maxval=20)
            save_state(G,step_i,output_dir,prefix="GFP",minval=0,maxval=2000)
            #save_state(R,step_i,output_dir,prefix="lasR",minval=0)
            #save_state(C,step_i,output_dir,prefix="Complex",minval=0)

            #update timer
            t_save_end = timer()-t_save
            save_state_time+= t_save_end


    #report
    print("\n###############\nRuntime Analysis\n###############")
    print("Solving ODE System:%.05f" % ode_time)
    print("Diffusion:%.05f" % diffusion_time)
    print("Saving Images/CSV:%.05f" % save_state_time )
    
def init_spot(u0,r=20,cx=50,cy=50,value=20):
    """
    Initial conditions - ring of inner radius r,
    # width dr centred at (cx,cy) (mm)
    #r, cx, cy = 2, 5, 5
    """
    r2 = r**2
    for i in range(nx):
        for j in range(ny):
            p2 = (i*dx-cx)**2 + (j*dy-cy)**2
            if p2 < r2:
                u0[i,j] = value
    return u0

def Diffusion(u0):
    """
    Propagate with forward-difference in time, central-difference in space
    to compute the Discrete Laplace Operator
    REF: https://scipython.com/book/chapter-7-matplotlib/examples/the-two-dimensional-diffusion-equation/
    """
    u1 = u0.copy()
    u1[1:-1, 1:-1] = u0[1:-1, 1:-1] + D * dt * (
          (u0[2:, 1:-1] - 2*u0[1:-1, 1:-1] + u0[:-2, 1:-1])/dx2
          + (u0[1:-1, 2:] - 2*u0[1:-1, 1:-1] + u0[1:-1, :-2])/dy2 )

    #u0 = u.copy()
    return u1

def integrate(input_fields,dx):
    """
    Function: Wrapper to transform data and perform integration.
    """
    G,A,R,C = input_fields
    Y = np.concatenate((G,A,R,C),axis=0).flatten() #must v=be 1D array
    dY = odeint(dReactions_dt,Y,[0.0,dt])[1] 
    dY = dY.reshape(4,nx,ny)
    G,A,R,C = dY[0,:,:],dY[1,:,:],dY[2,:,:],dY[3,:,:]
    
    return G,A,R,C
    
def dReactions_dt(Y,t):
    """
    Function to calculate the derivative of all reactions/protein interactions.
    Y = 1D array of flattened input variables. (M,N,4)->(1,MxNx4)
    t = time. not utilized
    """
    #reshape 1D vector to
    Y = Y.reshape(4,nx,ny)
    G = Y[0,:,:]
    A = Y[1,:,:]
    R = Y[2,:,:]
    C = Y[3,:,:]
    N = 2
    
    #Varaibles
    
    #GFP params
    a_G = 0.01
    b_G = 100.0
    k_CG = 5.0
    deg_G = 0.0289
    
    #AHL params
    #a_A = 0.00
    #b_A = 100.0
    #k_AA = 1.0
    deg_A = 0.012 # per min
    AN = A**N
    
    #lasR params
    a_R = 0.01
    b_R = 100.0
    k_CR = 5.0
    deg_R = 0.01
    
    #C (lasR=AHL complex) params
    CN = C**N
    k_C_on = 0.013
    k_C_off = 0.0001 #mins to hours
    deg_C = 0.0231 #basu 2005
        
    #compute diffeq. vectorization works
    
    G1 = a_G + b_G*CN/(k_CG**N+CN) - deg_G*G #k_CRand k_CG same promoter.
    A1 =  -(k_C_on*A*R) + (k_C_off*A*R) -deg_A*A
    R1 = a_R + b_R*CN/(k_CR**N+CN) - (k_C_on*A*R) + (k_C_off*A*R) - deg_R*R
    C1 = k_C_on*A*R - k_C_off*A*R - deg_C*C
    
    dY = np.concatenate((G1,A1,R1,C1)).flatten()
    return dY
    
    
    
def save_state(u,step_i,output_dir,prefix="plot",minval=None,maxval=None):
    """
    Function: Save an image of for a molecule of interest.
    Inputs:
    ----------
    u - array for the molecule
    output folder
    time
    """
    fig = plt.figure()
    # = plt.imshow(u.copy().T, cmap=plt.get_cmap('hot'))
    print("%s: min=%.03f max=%.03f" % (prefix,np.min(u),np.max(u)))
    if maxval==None or minval==None: 
        im = plt.imshow(u.T, cmap=plt.get_cmap('hot'), vmin=np.min(u),vmax=np.max(u),origin='lower')
    else:
        im = plt.imshow(u.T, cmap=plt.get_cmap('hot'), vmin=minval,vmax=maxval,origin='lower')
    #plt.set_axis_off()
    plt.title('{:.1f}h'.format(step_i*dt))
    #fig.subplots_adjust(right=0.85)
    cbar_ax = fig.add_axes([0.9, 0.15, 0.03, 0.7])
    cbar_ax.set_xlabel('$T$ / K', labelpad=20)
    fig.colorbar(im, cax=cbar_ax)
    
    # determine the output of output filenames
    if save_freq >=1:
        save_time = int(step_i*dt) #must be an integer save time
        save_path = os.path.join(output_dir,"{prefix}_{num:05d}".format(num=save_time,prefix=prefix))
    elif save_freq > 0 and save_freq < 1:
        save_time = float(step_i*dt) #must be an integer save time
        save_path = os.path.join(output_dir,"%s_%.05f" % (prefix,save_time))
    else:
        raise ValueError("Save frequencies error(save_frequency=%s)." % str(save_freq))
    
    #save image
    plt.savefig(save_path,dpi=72)
    plt.close()
    #save matrix
    

#########################  
t0 = timer()
main(dt_user=0.025,
     savepath="C:\\Dropbox (Personal)\\BU\\HyNeSs Lab\\biocps\\simulations\\reaction-diffusion\\sim1")
tend = timer()-t0
print("Runtime: %.05f" %tend)
